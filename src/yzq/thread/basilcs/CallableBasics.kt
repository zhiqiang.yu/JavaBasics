package yzq.thread

import java.lang.Thread.currentThread
import java.util.concurrent.Callable
import java.util.concurrent.FutureTask

fun main() {

    /**
     * callable 可以用来获取返回值，需要用FutureTask包装一下
     */
    val futureTask = FutureTask(Callable {
        println("FutureTask:${currentThread().name}")
        Thread.sleep(2000)
        return@Callable "这是返回值"
    })

    Thread(futureTask).start()

    val result = futureTask.get()//get方法会阻塞住线程

    println("result = ${result}")


}


