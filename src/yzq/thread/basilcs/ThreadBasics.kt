package yzq.thread

import java.lang.Thread.currentThread
import kotlin.concurrent.thread

/**
 * 线程基本使用的两种方式
 * 1.类继承Thread，实现run方法
 * 2.类实现Runnable接口，实现run方法
 */
fun main() {
    /**
     * 继承线程的方式
     */
    Method1().start()
    /**
     * 实现Runnable的方式
     */
    Thread(Method2()).start()

    /**
     * 匿名内部类的方式
     */
    thread { println("匿名内部类的方式${currentThread().name}") }
}


class Method1 : Thread() {
    override fun run() {
        println("继承thread的方式:${currentThread().name}")
    }
}

class Method2 : Runnable {
    override fun run() {
        println("实现runnable的方式:${currentThread().name}")
    }

}